# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/notification/common_event_service/event.gni")
import("//build/ohos.gni")

cflags = []

config("ces_services_config") {
  visibility = [ ":*" ]

  include_dirs = [ "//foundation/resourceschedule/ffrt/interfaces/kits" ]

  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
}

config("ces_services_public_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "${ces_services_path}/include",
    "//foundation/resourceschedule/ffrt/interfaces/kits",
  ]
}

cesfwk_services_sources = [
  "${ces_services_path}/src/ability_manager_death_recipient.cpp",
  "${ces_services_path}/src/ability_manager_helper.cpp",
  "${ces_services_path}/src/access_token_helper.cpp",
  "${ces_services_path}/src/bms_death_recipient.cpp",
  "${ces_services_path}/src/bundle_manager_helper.cpp",
  "${ces_services_path}/src/common_event_control_manager.cpp",
  "${ces_services_path}/src/common_event_manager_service.cpp",
  "${ces_services_path}/src/common_event_manager_service_ability.cpp",
  "${ces_services_path}/src/common_event_permission_manager.cpp",
  "${ces_services_path}/src/common_event_sticky_manager.cpp",
  "${ces_services_path}/src/common_event_subscriber_manager.cpp",
  "${ces_services_path}/src/common_event_support_mapper.cpp",
  "${ces_services_path}/src/event_report.cpp",
  "${ces_services_path}/src/inner_common_event_manager.cpp",
  "${ces_services_path}/src/ordered_event_handler.cpp",
  "${ces_services_path}/src/os_account_manager_helper.cpp",
  "${ces_services_path}/src/publish_manager.cpp",
  "${ces_services_path}/src/static_subscriber_connection.cpp",
  "${ces_services_path}/src/static_subscriber_data_manager.cpp",
  "${ces_services_path}/src/static_subscriber_manager.cpp",
  "${ces_services_path}/src/subscriber_death_recipient.cpp",
  "${ces_services_path}/src/system_time.cpp",
]

ohos_shared_library("cesfwk_services") {
  shlib_type = "sa"
  version_script = "libcesfwk_services.map"
  cflags_cc = []
  sources = cesfwk_services_sources

  configs = [ ":ces_services_config" ]

  public_configs = [ ":ces_services_public_config" ]

  deps = [
    "${ces_common_path}:libevent_common",
    "${ces_core_path}:cesfwk_core",
    "${ces_extension_path}:static_subscriber_ipc",
    "${ces_native_path}:cesfwk_innerkits",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_manager",
    "access_token:libaccesstoken_sdk",
    "access_token:libprivacy_sdk",
    "access_token:libtokenid_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hicollie_native:libhicollie",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  if (has_os_account_part) {
    cflags_cc += [ "-DHAS_OS_ACCOUNT_PART" ]
    external_deps += [ "os_account:os_account_innerkits" ]
  }

  if (has_hisysevent_part) {
    cflags_cc += [ "-DHAS_HISYSEVENT_PART" ]
    external_deps += [ "hisysevent:libhisysevent" ]
  }

  subsystem_name = "notification"
  part_name = "common_event_service"
}

# Note: Just for test
ohos_static_library("cesfwk_services_static") {
  cflags_cc = []
  sources = cesfwk_services_sources

  configs = [ ":ces_services_config" ]

  public_configs = [ ":ces_services_public_config" ]

  deps = [
    "${ces_common_path}:libevent_common",
    "${ces_core_path}:cesfwk_core",
    "${ces_extension_path}:static_subscriber_ipc",
    "${ces_native_path}:cesfwk_innerkits",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_manager",
    "access_token:libaccesstoken_sdk",
    "access_token:libprivacy_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  if (has_os_account_part) {
    cflags_cc += [ "-DHAS_OS_ACCOUNT_PART" ]
    external_deps += [ "os_account:os_account_innerkits" ]
  }

  if (has_hisysevent_part) {
    cflags_cc += [ "-DHAS_HISYSEVENT_PART" ]
    external_deps += [ "hisysevent:libhisysevent" ]
  }

  subsystem_name = "notification"
  part_name = "common_event_service"
}
